package dao;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class DAO {
    private static DAO instancia = null;
    private static String fuente = "datos/alumnos.txt";

    private DAO() { }

    public static DAO getInstancia() {
        if (instancia == null) {
            instancia = new DAO();
        }

        return instancia;
    }

    public List<String> getDatos() {
        List<String> datos = new ArrayList<>();

        try {
            datos = Files.readAllLines(Paths.get(fuente));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return datos;
    }

    public String get(int indice) {
        return getDatos().get(indice);
    }
}
