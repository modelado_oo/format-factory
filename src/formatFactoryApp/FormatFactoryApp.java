package formatFactoryApp;

import dao.DAO;
import fmwTransformadores.fabricasTransformadores.Transformadora;
import fmwTransformadores.fabricasTransformadores.TransformadoraPolo;
import fmwTransformadores.transformadores.Transformador;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Stream;

public class FormatFactoryApp {
    private static DAO dao = DAO.getInstancia();
    private static String RUTA_ARCHIVOS_GENERADOS = "datos/";

    public static void main(String[] args) {
        List<String> datos = dao.getDatos();
        String rutaTemp;

        i("Datos crudos:");
        i(datos);

        i("\nCreando la instancia de la transformadora Polo S.A. de C.V.");
        Transformadora factory = TransformadoraPolo.getInstancia();

        //region TXT
        rutaTemp = RUTA_ARCHIVOS_GENERADOS + "main.txt";
        i("Convirtiendo los datos y guardando archivo en " + rutaTemp + "...");
        factory.solicitarTransformacion(datos, rutaTemp, "txt");
        i("Transformador utilizado: " + factory.getTransformador());

        i("Archivo generado:");
        leerArchivo(rutaTemp);
        //endregion TXT

        //region CSV
        rutaTemp = RUTA_ARCHIVOS_GENERADOS + "main.csv";
        i("Convirtiendo los datos y guardando archivo en " + rutaTemp + "...");
        factory.solicitarTransformacion(datos, rutaTemp, "csv");
        i("Transformador utilizado: " + factory.getTransformador());

        i("Archivo generado:");
        leerArchivo(rutaTemp);
        //endregion CSV

        //region XML
        rutaTemp = RUTA_ARCHIVOS_GENERADOS + "main.xml";
        i("Convirtiendo los datos y guardando archivo en " + rutaTemp + "...");
        factory.solicitarTransformacion(datos, rutaTemp, "xml");
        i("Transformador utilizado: " + factory.getTransformador());

        i("Archivo generado:");
        leerArchivo(rutaTemp);
        //endregion XML

        //region JSON
        rutaTemp = RUTA_ARCHIVOS_GENERADOS + "main.json";
        i("Convirtiendo los datos y guardando archivo en " + rutaTemp + "...");
        factory.solicitarTransformacion(datos, rutaTemp, "json");
        i("Transformador utilizado: " + factory.getTransformador());

        i("Archivo generado:");
        leerArchivo(rutaTemp);
        //endregion JSON
    }

    private static void leerArchivo(String ruta) {
        try (Stream<String> stream = Files.lines(Paths.get(ruta))) {
            stream.forEach(FormatFactoryApp::i);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void i(Object o) {
        System.out.println(o);
    }
}
