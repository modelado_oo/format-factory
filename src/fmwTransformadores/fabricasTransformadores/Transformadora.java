package fmwTransformadores.fabricasTransformadores;

import fmwTransformadores.transformadores.Transformador;

import java.util.List;

public abstract class Transformadora {
    private Transformador transformador;

    public void solicitarTransformacion(List<String> lineas, String ruta, String tipoTransformacion) {
        transformador = crearTransformador(tipoTransformacion);

        transformador.transformarYGuardar(lineas, ruta);
    }

    public Transformador getTransformador() {
        return transformador;
    }

    protected abstract Transformador crearTransformador(String tipoTransformador);
}
