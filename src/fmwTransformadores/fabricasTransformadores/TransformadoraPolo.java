package fmwTransformadores.fabricasTransformadores;

import fmwTransformadores.transformadores.*;

public class TransformadoraPolo extends Transformadora {
    //region Singleton
    private static TransformadoraPolo instancia;

    private TransformadoraPolo() {
    }

    public static TransformadoraPolo getInstancia() {
        if (instancia == null) {
            instancia = new TransformadoraPolo();
        }

        return instancia;
    }
    //endregion

    @Override
    public Transformador crearTransformador(String tipoTransformador) {
        tipoTransformador = tipoTransformador.toLowerCase();
        Transformador transformador;

        switch (tipoTransformador) {
            case "csv": {
                transformador = new TransformadorCSV();
                break;
            }
            case "txt": {
                transformador = new TransformadorTXT();
                break;
            }
            case "xml": {
                transformador = new TransformadorXML();
                break;
            }
            case "json": {
                transformador = new TransformadorJSON();
                break;
            }
            default:
                throw new IllegalArgumentException("El tipo de transformador no existe");
        }

        return transformador;
    }
}
