package fmwTransformadores.transformadores;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class TransformadorCSV implements Transformador {

    @Override
    public boolean transformarYGuardar(List<String> lineas, String ruta) {
        boolean guardado = false;

        try {
            FileWriter fw = new FileWriter(ruta);

            lineas.forEach(linea -> {
                try {
                    String lineaModificada = linea.replace('|', ',');

                    fw.append(lineaModificada).append("\r\n");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });

            fw.close();
            guardado = true;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return guardado;
    }

}
