package fmwTransformadores.transformadores;

import com.google.gson.*;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class TransformadorJSON implements Transformador {
    @SuppressWarnings("unchecked")
    @Override
    public boolean transformarYGuardar(List<String> lineas, String ruta) {
        boolean guardado = false;

//        JSONObject jsonObjectAlumnos = new JSONObject();
//        JSONArray jsonArrayAlumnos = new JSONArray();
//        JSONObject jsonAlumno;

        Gson gson= new Gson();
        JsonObject jsonObjectAlumno;

        JsonArray jsonArrayAlumnos = new JsonArray();
        for (String linea1 : lineas) {
            String[] atributos = linea1.split("\\|");

            jsonObjectAlumno = new JsonObject();

            jsonObjectAlumno.add("Numero_de_Control", new JsonPrimitive(atributos[0]));
            jsonObjectAlumno.add("Nombre", new JsonPrimitive(atributos[1]));
            jsonObjectAlumno.add("Apellido_Paterno", new JsonPrimitive(atributos[2]));
            jsonObjectAlumno.add("Apellido_Materno", new JsonPrimitive(atributos[3]));
            jsonObjectAlumno.add("Edad", new JsonPrimitive(atributos[4]));
            jsonObjectAlumno.add("Sexo", new JsonPrimitive(atributos[5]));
            jsonObjectAlumno.add("Carrera", new JsonPrimitive(atributos[6]));
            jsonObjectAlumno.add("Semestre", new JsonPrimitive(atributos[7]));

            jsonArrayAlumnos.add(jsonObjectAlumno);
        }

//        for (String linea : lineas) {
//            String[] attEstudiante = linea.split("\\|");
//            jsonAlumno = new JSONObject();
//
//            jsonAlumno.put("Numero_de_control", attEstudiante[0]);
//            jsonAlumno.put("Nombre",attEstudiante[1]);
//            jsonAlumno.put("Apellido_Paterno",attEstudiante[2]);
//            jsonAlumno.put("Apellido_Materno",attEstudiante[3]);
//            jsonAlumno.put("Edad",attEstudiante[4]);
//            jsonAlumno.put("Sexo",attEstudiante[5]);
//            jsonAlumno.put("Carrera",attEstudiante[6]);
//            jsonAlumno.put("Semestre",attEstudiante[7]);
//
//            jsonArrayAlumnos.add(jsonAlumno);
//        }
//
//
//        jsonObjectAlumnos.put("Alumnos", jsonArrayAlumnos);

        try (FileWriter file = new FileWriter(ruta)) {
//            file.write(jsonObjectAlumnos.toJSONString());
            file.write(gson.toJson(jsonArrayAlumnos));
            guardado = true;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return guardado;
    }
}
