package fmwTransformadores.transformadores;

import java.util.List;

public interface Transformador {
    boolean transformarYGuardar(List<String> lineas, String ruta);
}
