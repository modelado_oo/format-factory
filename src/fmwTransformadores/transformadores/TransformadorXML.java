package fmwTransformadores.transformadores;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.List;

public class TransformadorXML implements Transformador {
    @Override
    public boolean transformarYGuardar(List<String> lineas, String ruta) {
        boolean guardado = false;

        Document dom;
        Element elemEstudiante;
        Element elemAux;

        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();

        try {
            DocumentBuilder documentBuilder = builderFactory.newDocumentBuilder();
            dom = documentBuilder.newDocument();

            Element elemRaiz = dom.createElement("Estudiantes");

            for (String linea : lineas) {
                String[] attEstudiante = linea.split("\\|");

                elemEstudiante = dom.createElement("Estudiante");

                elemAux = dom.createElement("Numero_de_Control");
                elemAux.appendChild(dom.createTextNode(attEstudiante[0]));

                elemEstudiante.appendChild(elemAux);

                elemAux = dom.createElement("Nombre");
                elemAux.appendChild(dom.createTextNode(attEstudiante[1]));

                elemEstudiante.appendChild(elemAux);

                elemAux = dom.createElement("Apellido_Paterno");
                elemAux.appendChild(dom.createTextNode(attEstudiante[2]));

                elemEstudiante.appendChild(elemAux);

                elemAux = dom.createElement("Apellido_Materno");
                elemAux.appendChild(dom.createTextNode(attEstudiante[3]));

                elemEstudiante.appendChild(elemAux);

                elemAux = dom.createElement("Edad");
                elemAux.appendChild(dom.createTextNode(attEstudiante[4]));

                elemEstudiante.appendChild(elemAux);

                elemAux = dom.createElement("Sexo");
                elemAux.appendChild(dom.createTextNode(attEstudiante[5]));

                elemEstudiante.appendChild(elemAux);

                elemAux = dom.createElement("Carrera");
                elemAux.appendChild(dom.createTextNode(attEstudiante[6]));

                elemEstudiante.appendChild(elemAux);

                elemAux = dom.createElement("Semestre");
                elemAux.appendChild(dom.createTextNode(attEstudiante[7]));

                elemEstudiante.appendChild(elemAux);

                elemRaiz.appendChild(elemEstudiante);
            }

            dom.appendChild(elemRaiz);

            try {
                Transformer tr = TransformerFactory.newInstance().newTransformer();
                tr.setOutputProperty(OutputKeys.INDENT, "yes");
                tr.setOutputProperty(OutputKeys.METHOD, "xml");
                tr.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
                tr.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

                tr.transform(new DOMSource(dom), new StreamResult(new FileOutputStream(ruta)));
                guardado = true;
            } catch (FileNotFoundException | TransformerException e1) {
                e1.printStackTrace();
            }
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }

        return guardado;
    }
}
