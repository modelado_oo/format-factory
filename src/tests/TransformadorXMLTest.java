package tests;

import fmwTransformadores.fabricasTransformadores.Transformadora;
import fmwTransformadores.fabricasTransformadores.TransformadoraPolo;
import fmwTransformadores.transformadores.Transformador;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class TransformadorXMLTest {
    Transformadora transformadora;

    @BeforeEach
    void setUp() {
        transformadora = TransformadoraPolo.getInstancia();
    }

    // Prueba con solo un alumno.
    @Test
    void transformarYGuardar() {
        String ruta = "src/tests/test.xml";

        List<String> alumnos = new ArrayList<>();
        alumnos.add("0001|Jorge Abraham|Romero|Polo|24|H|MCC|2");

        transformadora.solicitarTransformacion(alumnos, ruta, "xml");

        List<String> lineasXMLEsperadas = new ArrayList<>();
        lineasXMLEsperadas.add("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>");
        lineasXMLEsperadas.add("<Estudiantes>");
        lineasXMLEsperadas.add("    <Estudiante>");
        lineasXMLEsperadas.add("        <Numero_de_Control>0001</Numero_de_Control>");
        lineasXMLEsperadas.add("        <Nombre>Jorge Abraham</Nombre>");
        lineasXMLEsperadas.add("        <Apellido_Paterno>Romero</Apellido_Paterno>");
        lineasXMLEsperadas.add("        <Apellido_Materno>Polo</Apellido_Materno>");
        lineasXMLEsperadas.add("        <Edad>24</Edad>");
        lineasXMLEsperadas.add("        <Sexo>H</Sexo>");
        lineasXMLEsperadas.add("        <Carrera>MCC</Carrera>");
        lineasXMLEsperadas.add("        <Semestre>2</Semestre>");
        lineasXMLEsperadas.add("    </Estudiante>");
        lineasXMLEsperadas.add("</Estudiantes>");

        try (Stream<String> stream = Files.lines(Paths.get(ruta))) {
            Iterator<String> iterArchivo = stream.iterator();
            Iterator<String> iterLineas = lineasXMLEsperadas.iterator();

            while(iterArchivo.hasNext() && iterLineas.hasNext()) {
                assertEquals(iterArchivo.next(), iterLineas.next());
            }

            assert !iterArchivo.hasNext() && !iterLineas.hasNext();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}