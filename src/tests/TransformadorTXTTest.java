package tests;

import dao.DAO;
import fmwTransformadores.fabricasTransformadores.Transformadora;
import fmwTransformadores.fabricasTransformadores.TransformadoraPolo;
import fmwTransformadores.transformadores.Transformador;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class TransformadorTXTTest {
    DAO dao;
    Transformadora transformadora;

    @BeforeEach
    void setUp() {
        dao = DAO.getInstancia();
        transformadora = TransformadoraPolo.getInstancia();
    }

    @Test
    void transformarYGuardar() {
        String ruta = "src/tests/test.txt";
        transformadora.solicitarTransformacion(dao.getDatos(), ruta, "txt");
        
        List<String> lineas = new ArrayList<>();
        lineas.add("0001\tJorge Abraham\tRomero\tPolo\t24\tH\tMCC\t2");
        lineas.add("0002\tJuan Ramón\tValenzuela\tBarraza\t25\tH\tMCC\t2");
        lineas.add("0003\tHéctor Manuel\tCárdenas\tLópez\t25\tH\tMCC\t2");
        lineas.add("0004\tFrancisco\tGonzáles\tHernández\t36\tH\tMCC\t2");
        lineas.add("0005\tEmmanuel Francisco\tHernández\tRamírez\t26\tH\tMCC\t2");
        lineas.add("0006\tBrandon Antonio\tCárdenas\tSainz\t25\tH\tMCC\t2");

        try (Stream<String> stream = Files.lines(Paths.get(ruta))) {
            Iterator<String> iterArchivo = stream.iterator();
            Iterator<String> iterLineas = lineas.iterator();

            while(iterArchivo.hasNext() && iterLineas.hasNext()) {
                assertEquals(iterArchivo.next(), iterLineas.next());
            }

            assert !iterArchivo.hasNext() && !iterLineas.hasNext();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}