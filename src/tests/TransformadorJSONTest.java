package tests;

import fmwTransformadores.fabricasTransformadores.Transformadora;
import fmwTransformadores.fabricasTransformadores.TransformadoraPolo;
import fmwTransformadores.transformadores.Transformador;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class TransformadorJSONTest {
    Transformadora transformadora;

    @BeforeEach
    void setUp() {
        transformadora = TransformadoraPolo.getInstancia();
    }

    @Test
    void transformarYGuardar() {
        String ruta = "src/tests/test.json";
        List<String> lineas = new ArrayList<>();
        lineas.add("0001|Jorge Abraham|Romero|Polo|24|H|MCC|2");
        lineas.add("0002|Juan Ramón|Valenzuela|Barraza|25|H|MCC|2");

        transformadora.solicitarTransformacion(lineas, ruta, "json");

        try (Stream<String> stream = Files.lines(Paths.get(ruta))) {
            stream.findFirst().ifPresent(json -> {

                String stringBuilder = "[{\"Numero_de_Control\":\"0001\"," +
                        "\"Nombre\":\"Jorge Abraham\"," +
                        "\"Apellido_Paterno\":\"Romero\"," +
                        "\"Apellido_Materno\":\"Polo\"," +
                        "\"Edad\":\"24\"," +
                        "\"Sexo\":\"H\"," +
                        "\"Carrera\":\"MCC\"," +
                        "\"Semestre\":\"2\"}," +
                        "{\"Numero_de_Control\":\"0002\"," +
                        "\"Nombre\":\"Juan Ramón\"," +
                        "\"Apellido_Paterno\":\"Valenzuela\"," +
                        "\"Apellido_Materno\":\"Barraza\"," +
                        "\"Edad\":\"25\"," +
                        "\"Sexo\":\"H\"," +
                        "\"Carrera\":\"MCC\"," +
                        "\"Semestre\":\"2\"}]";

                assertEquals(stringBuilder, json);
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}