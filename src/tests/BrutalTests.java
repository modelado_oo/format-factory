package tests;

import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonPrimitive;
import dao.DAO;
import fmwTransformadores.transformadores.*;
import org.w3c.dom.*;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

public class BrutalTests {
    public static void main(String[] args) {
//        Transformador transformador = new TransformadorCSV();
//        DAO dao = DAO.getInstancia();
//        transformador.transformarYGuardar(dao.getDatos(), "datos.csv");
//
//        transformador = new TransformadorTXT();
//        transformador.transformarYGuardar(dao.getDatos(), "datos.txt");
//
//        guardarXML("Hola.xml");
//
//        Transformador transformador = new TransformadorXML();
//        DAO dao = DAO.getInstancia();
//        transformador.transformarYGuardar(dao.getDatos(), "datos.xml");

        Transformador transformador = new TransformadorJSON();
        DAO dao = DAO.getInstancia();
        List<String> prueba = new ArrayList<>();
        prueba.add(dao.get(0));
        prueba.add(dao.get(1));
        transformador.transformarYGuardar(prueba, "src/tests/brutalJSON.json");

    }

    public static void guardarXML(String ruta) {
        Document dom;
        Element e;

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

        try {
            DocumentBuilder db = dbf.newDocumentBuilder();
            dom = db.newDocument();

            Element rootEle = dom.createElement("roles");

            e = dom.createElement("role1");
            e.appendChild(dom.createTextNode("Rol 1 alv"));
            rootEle.appendChild(e);

            e = dom.createElement("role2");
            e.appendChild(dom.createTextNode("Role 2 alv"));
            rootEle.appendChild(e);

            e = dom.createElement("role3");
            e.appendChild(dom.createTextNode("Role 3 alv"));
            rootEle.appendChild(e);

            e = dom.createElement("role4");
            e.appendChild(dom.createTextNode("Role 4 alv"));
            rootEle.appendChild(e);

            dom.appendChild(rootEle);

            try {
                Transformer tr = TransformerFactory.newInstance().newTransformer();
                tr.setOutputProperty(OutputKeys.INDENT, "yes");
                tr.setOutputProperty(OutputKeys.METHOD, "xml");
                tr.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
                //tr.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, "roles.dtd");
                tr.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

                tr.transform(new DOMSource(dom), new StreamResult(new FileOutputStream(ruta)));
            } catch (FileNotFoundException | TransformerException e1) {
                e1.printStackTrace();
            }
        } catch (ParserConfigurationException e1) {
            e1.printStackTrace();
        }
    }
}
