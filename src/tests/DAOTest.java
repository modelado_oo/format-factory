package tests;

import dao.DAO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class DAOTest {
    DAO dao;

    @BeforeEach
    void setUp() {
        dao = DAO.getInstancia();
    }

    @Test
    void getInstancia() {
        DAO nuevoDao = DAO.getInstancia();
        assertSame(dao, nuevoDao);
    }

    @Test
    void getDatos() {
        List<String> lineas = new ArrayList<>();
        lineas.add("0001|Jorge Abraham|Romero|Polo|24|H|MCC|2");
        lineas.add("0002|Juan Ramón|Valenzuela|Barraza|25|H|MCC|2");
        lineas.add("0003|Héctor Manuel|Cárdenas|López|25|H|MCC|2");
        lineas.add("0004|Francisco|Gonzáles|Hernández|36|H|MCC|2");
        lineas.add("0005|Emmanuel Francisco|Hernández|Ramírez|26|H|MCC|2");
        lineas.add("0006|Brandon Antonio|Cárdenas|Sainz|25|H|MCC|2");

        assertEquals(lineas, dao.getDatos());
    }
}