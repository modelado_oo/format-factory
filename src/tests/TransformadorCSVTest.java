package tests;

import dao.DAO;
import fmwTransformadores.fabricasTransformadores.Transformadora;
import fmwTransformadores.fabricasTransformadores.TransformadoraPolo;
import fmwTransformadores.transformadores.Transformador;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class TransformadorCSVTest {
    DAO dao;
    Transformadora transformadora;

    @BeforeEach
    void setUp() {
        dao = DAO.getInstancia();
        transformadora = TransformadoraPolo.getInstancia();
    }

    @Test
    void transformarYGuardar() {
        String ruta = "src/tests/test.csv";
        transformadora.solicitarTransformacion(dao.getDatos(), ruta, "csv");

        List<String> lineas = new ArrayList<>();
        lineas.add("0001,Jorge Abraham,Romero,Polo,24,H,MCC,2");
        lineas.add("0002,Juan Ramón,Valenzuela,Barraza,25,H,MCC,2");
        lineas.add("0003,Héctor Manuel,Cárdenas,López,25,H,MCC,2");
        lineas.add("0004,Francisco,Gonzáles,Hernández,36,H,MCC,2");
        lineas.add("0005,Emmanuel Francisco,Hernández,Ramírez,26,H,MCC,2");
        lineas.add("0006,Brandon Antonio,Cárdenas,Sainz,25,H,MCC,2");

        try (Stream<String> stream = Files.lines(Paths.get(ruta))) {
            Iterator<String> iterArchivo = stream.iterator();
            Iterator<String> iterLineas = lineas.iterator();

            while (iterArchivo.hasNext() && iterLineas.hasNext()) {
                assertEquals(iterArchivo.next(), iterLineas.next());
            }

            assert !iterArchivo.hasNext() && !iterLineas.hasNext();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}