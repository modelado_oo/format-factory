package tests;

import fmwTransformadores.fabricasTransformadores.TransformadoraPolo;
import fmwTransformadores.transformadores.Transformador;
import fmwTransformadores.transformadores.TransformadorCSV;
import fmwTransformadores.transformadores.TransformadorTXT;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TransformadoraPoloTest {
    TransformadoraPolo factory;

    @BeforeEach
    void setUp() {
        factory = TransformadoraPolo.getInstancia();
    }

    @Test
    void getInstancia() {
        TransformadoraPolo factory2 = TransformadoraPolo.getInstancia();
        assertSame(factory, factory2);
    }

    @Test
    void crearTransformadorCSV() {
        Transformador transformadorCSV = factory.crearTransformador("CSV");
        assertEquals(transformadorCSV.getClass(), TransformadorCSV.class);
    }

    @Test
    void crearTransformadorTXT() {
        Transformador transformadorTXT = factory.crearTransformador("tXT");
        assertEquals(transformadorTXT.getClass(), TransformadorTXT.class);
    }
}